import React from "react";
import PropTypes from "prop-types";
import "./button.css";

export const Button = ({
  variant,
  size,
  label,
  onClick,
  disabled,
  ...props
}) => {
  return (
    <button
      type="button"
      className={[
        "button",
        `button--${size}`,
        `variant--${variant}`,
        disabled && "button--disabled",
      ].join(" ")}
      style={{ ...props }}
      onClick={onClick}
      disabled={disabled}
    >
      {label}
    </button>
  );
};

Button.propTypes = {
  /**
   * What background color to use
   */
  variant: PropTypes.oneOf(["Apps", "Eole", "Rizomo"]),
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(["small", "medium", "large"]),
  /**
   * Button contents
   */
  label: PropTypes.string.isRequired,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
  /**
   * Disabled the button
   */
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  variant: "Apps",
  size: "medium",
  onClick: () => {},
  disabled: false,
};
