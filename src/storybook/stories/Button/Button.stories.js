/* eslint-disable import/no-anonymous-default-export */
import { Button } from "./Button";

// More on how to set up stories at: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/Button",
  component: Button,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/react/configure/story-layout
    layout: "centered",
  },
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ["autodocs"],
  argTypes: {
    variant: {
      options: ["Apps", "Eole", "Rizomo"],
      control: { type: "select" },
    },
    disabled: {
      control: { type: "boolean" },
    },
  },
};

// More on writing stories with args: https://storybook.js.org/docs/react/writing-stories/args
export const Apps = {
  args: {
    label: "Apps button",
    variant: "Apps",
  },
};

export const Eole = {
  args: {
    label: "Eole button",
    variant: "Eole",
  },
};

export const Rizomo = {
  args: {
    label: "Rizomo button",
    variant: "Rizomo",
  },
};

export const Props = {
  args: {
    label: "Props button",
    disabled: true,
    onClick: () => {
      alert("props");
    },
  },
};
