/* eslint-disable import/no-anonymous-default-export */
import { Text } from "./Text";

export default {
  title: "Example/Text",
  component: Text,
  tags: ["autodocs"],
  argTypes: {},
};

export const Default = {
  args: {
    label: "Text",
  },
};

export const WithProps = {
  args: {
    label: "Props",
    color: "red",
  },
};
