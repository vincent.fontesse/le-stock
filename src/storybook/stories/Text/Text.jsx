import React from "react";
import PropTypes from "prop-types";

export const Text = ({ label, ...props }) => {
  const minLength = 2;
  return (
    <div style={{ border: label.length < minLength && "1px solid red" }}>
      {label}
    </div>
  );
};

Text.propTypes = {
  /**
   * Super titre
   */
  label: PropTypes.string.isRequired,
};

Text.defaultProps = {
  label: "",
};
